﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        :BaseEntity
    {
        public Preference()
        {
            
        }
        public string Name { get; set; }
        public List<CustomerPreference> CustomerPreference { get; set; }
    }
}