﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Exceptions
{
    public class EntityNotFoundException : Exception
    {
        public EntityNotFoundException(string message) : base(message) { }
    }
}