﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;

        public CustomersController(IRepository<Customer> customerRepository)
        {
            _customerRepository = customerRepository;
        }
        
        /// <summary>
        /// Получить данные всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            //TODO: Добавить получение списка клиентов
            return Ok((await _customerRepository.GetAllAsync()).Select(GetCustomerShortResponse));
        }
        
        /// <summary>
        /// Получить данные клиента по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            //TODO: Добавить получение клиента вместе с выданными ему промомкодами
            var customer = await _customerRepository.GetByIdAsync(id);
            return Ok(new CustomerResponse()
            {
                Email = customer.Email,
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                PromoCodes = customer.PromoCodes?.Select(p=> new PromoCodeShortResponse()
                {
                    Code = p.Code,
                    Id = p.Id,
                    BeginDate = p.BeginDate.ToString("s"),
                    EndDate = p.EndDate.ToString("s"),
                    PartnerName = p.PartnerName,
                    ServiceInfo = p.ServiceInfo
                }).ToList(),
                Preferences = customer.CustomerPreference.Select(p=> new PreferenceResponse()
                    {
                        Id = p.Preference.Id,
                        Name = p.Preference.Name
                    }
                ).ToList()
            });
        }
        
        /// <summary>
        /// Добавить клиента
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            //TODO: Добавить создание нового клиента вместе с его предпочтениями
            var customerId = Guid.NewGuid();
            var customer = new Customer()
            {
                Id = customerId,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                CustomerPreference = request.PreferenceIds.Select(x => new CustomerPreference()
                    {CustomerId = customerId, PreferenceId = x}).ToList()
            };
            
            return Ok(GetCustomerShortResponse(await _customerRepository.AddAsync(customer))); 
        }
        
        /// <summary>
        /// Изменить клиента
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            //TODO: Обновить данные клиента вместе с его предпочтениями
            var customer = new Customer()
            {
                Id = id,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                CustomerPreference = request.PreferenceIds.Select(x => new CustomerPreference()
                    {CustomerId = id, PreferenceId = x}).ToList()
            };
            
            return Ok(GetCustomerShortResponse(await _customerRepository.UpdateAsync(customer))); 
        }
        
        /// <summary>
        /// Удалить клиента
        /// </summary>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            //TODO: Удаление клиента вместе с выданными ему промокодами
            return Ok(GetCustomerShortResponse(await _customerRepository.DeleteAsync(id)));
        }

        private CustomerShortResponse GetCustomerShortResponse(Customer customer)
        {
            return new CustomerShortResponse()
            {
                Email = customer.Email,
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName
            };
        }
    }
}