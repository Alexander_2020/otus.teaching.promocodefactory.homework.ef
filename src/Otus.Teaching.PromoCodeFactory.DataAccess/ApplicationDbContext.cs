﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options): base(options)
        {
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<CustomerPreference>(entity =>
            {
                entity.HasKey(c => new {c.CustomerId, c.PreferenceId});
                entity.HasOne(c => c.Customer)
                    .WithMany(p => p.CustomerPreference)
                    .HasForeignKey(c => c.CustomerId);
                entity.HasOne(c => c.Preference)
                    .WithMany(p => p.CustomerPreference)
                    .HasForeignKey(c => c.PreferenceId);
            });
            
            modelBuilder.Entity<Customer>(entity =>
            {
                entity.HasMany(c => c.PromoCodes)
                    .WithOne(p=>p.Customer)
                    .HasForeignKey(c=>c.CustomerId)
                    .OnDelete(DeleteBehavior.Cascade);
            });
        }
    }
}