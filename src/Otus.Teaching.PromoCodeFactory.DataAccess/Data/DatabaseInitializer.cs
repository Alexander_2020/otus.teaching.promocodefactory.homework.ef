﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class DatabaseInitializer
    {
        public static async void Seed(IServiceProvider serviceProvider)
        {
            using var scope = serviceProvider.CreateScope();
            await using var context = scope.ServiceProvider.GetService<ApplicationDbContext>();

            if (context == null) return;
            
            context.Database.Migrate();

            foreach (var preference in FakeDataFactory.Preferences)
            {
                if (context.Preferences.Any(p => p.Id == preference.Id)) continue;

                await context.AddAsync(preference);
            }
            await context.SaveChangesAsync();

            foreach (var role in FakeDataFactory.Roles)
            {
                if (context.Roles.Any(p => p.Id == role.Id)) continue;

                await context.AddAsync(role);
            }
            await context.SaveChangesAsync();
            
            foreach (var employee in FakeDataFactory.Employees)
            {
                if (context.Employees.Any(p => p.Id == employee.Id)) continue;
                
                if (employee.Role != null)
                {
                    employee.Role = await context.Roles.FirstOrDefaultAsync(r => r.Name == employee.Role.Name);
                }

                await context.AddAsync(employee);
            }
            await context.SaveChangesAsync();

            foreach (var customer in FakeDataFactory.Customers)
            {
                if (context.Customers.Any(p => p.Id == customer.Id)) continue;

                if (customer.CustomerPreference != null)
                {
                    foreach (var customerPreference in customer.CustomerPreference)
                    {
                        customerPreference.Preference = await context.Preferences.FirstOrDefaultAsync(
                            p => p.Id == customerPreference.PreferenceId);
                        customerPreference.CustomerId = customer.Id;
                    }
                }
                await context.AddAsync(customer);
            }
            
            await context.SaveChangesAsync();
           
            foreach (var promoCode in FakeDataFactory.PromoCodes)
            {
                if (context.PromoCodes.Any(p => p.Id == promoCode.Id)) continue;

                if(promoCode.PartnerManager != null)
                    promoCode.PartnerManager =
                        await context.Employees.FirstOrDefaultAsync(e => e.Id == promoCode.PartnerManager.Id);
                
                if(promoCode.Customer != null)
                    promoCode.Customer =
                        await context.Customers.FirstOrDefaultAsync(c => c.Id == promoCode.Customer.Id);
                
                if(promoCode.Preference != null)
                    promoCode.Preference =
                        await context.Preferences.FirstOrDefaultAsync(p => p.Id == promoCode.Preference.Id);
                
                await context.AddAsync(promoCode);
            }
            
            await context.SaveChangesAsync();
        }
    }
}