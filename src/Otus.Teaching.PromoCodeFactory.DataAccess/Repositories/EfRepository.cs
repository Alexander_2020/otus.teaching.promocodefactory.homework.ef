﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Exceptions;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public EfRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }
        public async Task<T> AddAsync(T item)
        {
            await _applicationDbContext.AddAsync(item);
            await _applicationDbContext.SaveChangesAsync();
            return item;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _applicationDbContext.Set<T>().ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var entity = await _applicationDbContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);

            foreach (var property in _applicationDbContext.Model.FindEntityType(typeof(T)).GetNavigations())
            {
                if(property.FieldInfo.FieldType.IsGenericType)
                    await _applicationDbContext.Entry(entity).Collection(property.Name).LoadAsync();
                else
                    await _applicationDbContext.Entry(entity).Reference(property.Name).LoadAsync();
            }
            
            return entity;
        }

        public async Task<T> UpdateAsync(T item)
        {
            var entity = await GetByIdAsync(item.Id);
            if (entity == null) throw new EntityNotFoundException($"entity with id = {item.Id} not found!");

            var r = _applicationDbContext.Set<T>().Update(item);
            var rez = await _applicationDbContext.SaveChangesAsync();
            return entity;
        }

        public async Task<T> DeleteAsync(Guid id)
        {
            var entity = await GetByIdAsync(id);
            if (entity == null) throw new EntityNotFoundException($"entity with id = {id} not found!");
            
            _applicationDbContext.Set<T>().Remove(entity);
            await _applicationDbContext.SaveChangesAsync();
            return entity;
        }
    }
}